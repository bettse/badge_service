require 'yaml'
require 'forecast_io'
require 'awesome_print'


class Forecast
  def initialize(lat = nil, long = nil)
    config = YAML.load(File.read('config/forecast.yml'))
    ForecastIO.api_key = ENV['FORECAST_API_KEY']
    @lat = lat || config['lat']
    @long = long || config['long']
  end

  def get
    now = forecast.currently
    soon = forecast.minutely

    OpenStruct.new({
      current_temp: "#{now.temperature.round}",
      current_icon: now.icon,
      current_desc: now.summary,
      next_icon: soon.icon,
      next_desc: soon.summary,
    })
  end

  def forecast
    @forecast ||= ForecastIO.forecast(@lat, @long, {exclude: "daily,alerts,flags"})
  end

end
