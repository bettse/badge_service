########
# app.rb
#

require 'sinatra/base'
require "sinatra/reloader"
require './lib/forecast'

class XbmApp < Sinatra::Base
  #These are 2 less than the real screen so there is a single pixel border around all sides
  WIDTH  = 262
  HEIGHT = 174

  set :root, File.dirname(__FILE__)

  configure :development do
    register Sinatra::Reloader
  end

  configure do
    redisUri = ENV["REDIS_URL"] || 'redis://localhost:6379'
    uri = URI.parse(redisUri)
    $redis = Redis.new(host: uri.host, port: uri.port, password: uri.password)
  end

  before do
    #Disabling epd check until it can be agent-id specific
    return #if ['/epd', '/'].include?(request.path_info) || settings.development?
    begin
      busy = $redis.get('epd_busy') || 'false'
      halt(503) if busy == 'true'
    rescue Redis::BaseConnectionError => e
      ::NewRelic::Agent.notice_error(e)
      halt(500)
    rescue SocketError => e
      ::NewRelic::Agent.notice_error(e)
      halt(500)
    end
  end

  get '/' do
    'OK'
  end

  post '/epd' do
    body = JSON.parse(request.body.read.to_s)
    $redis.set('epd_busy', body['state'])
  end

  get '/:agent/weather' do
    agent = params[:agent]
    lat = params[:lat]
    lon = params[:lon] || params[:long]

    image = weather(lat, lon)

    send_image(image, agent)

    #Give the user the png version
    content_type 'image/png'
    image.format = 'png'
    image.to_blob
  end

  post '/weather' do
    body = JSON.parse(request.body.read.to_s)
    lat = body['lat']
    lon = body['lon'] || body['long']
    wifi = body['wifi']

    lat, lon = get_location(wifi) if wifi

    image = weather(lat, lon)

    pixels = image.rotate(180).negate.export_pixels(0, 0, WIDTH+2, HEIGHT+2, 'I')
    interlace(pixels)
  end

  #array of wifi data hashes from imp
  #https://gist.github.com/electricimp/9ad7db2f391058fa742f
  def get_location(wifi)
    options = {
      query: {
        browser: 'electric-imp',
        sensor: false,
        wifi: wifi.collect{|ap| "mac:#{add_colons(ap['bssid'])}|ss:#{ap['rssi']}"}
      }
    }
    url = "https://maps.googleapis.com/maps/api/browserlocation/json"

    res = HTTParty.get(url, options)
    if (res.code == 200)
      body = JSON.parse(res.body)
      location = body['location']
      return location['lat'], location['lng'] if location
    end
    return nil, nil
  end

  def add_colons(bssid)
    bssid.chars.each_slice(2).map{|byte| byte.join}.join(":")
  end

  def weather(lat, lon)
    render_forecast(Forecast.new(lat, lon).get)
  end

  def render_forecast(forecast)
    background = Magick::Image.new(WIDTH, HEIGHT) do
      self.antialias = false
    end

    text = Magick::Image.read("caption:Currently:     ") do
      self.antialias = false
      self.fill = 'black'
      self.stroke = 'black'
      self.background_color = 'none'
      self.font = "Helvetica"
      self.size = "#{WIDTH}x#{HEIGHT/3}"
    end.first
    background.composite!(text, Magick::NorthWestGravity, Magick::OverCompositeOp)

    icon = Magick::Image.read("public/climacons/#{forecast.current_icon}.png")do 
      self.antialias = false
    end.first.resize_to_fit(WIDTH/2,HEIGHT/2).transparent('white')
    background.composite!(icon, Magick::WestGravity, Magick::OverCompositeOp)

    temp = Magick::Image.read("caption:#{forecast.current_temp}°") do
      self.antialias = false
      self.fill = 'black'
      self.stroke = 'black'
      self.background_color = 'none'
      self.font = "Helvetica"
      self.size = "#{WIDTH/2}x#{HEIGHT/4}"
    end.first

    desc = Magick::Image.read("caption:#{forecast.current_desc}") do
      self.antialias = false
      self.fill = 'black'
      self.stroke = 'black'
      self.background_color = 'none'
      self.font = "Helvetica"
      self.size = "#{WIDTH/2}x#{HEIGHT/4}"
    end.first

    quad = Magick::Image.new(WIDTH/2,HEIGHT/2)
    quad.composite!(temp, Magick::NorthGravity, Magick::OverCompositeOp)
    quad.composite!(desc, Magick::SouthGravity, Magick::OverCompositeOp)

    background.composite!(quad, Magick::EastGravity, Magick::OverCompositeOp)

    icon = Magick::Image.read("public/climacons/#{forecast.next_icon}.png") do
      self.antialias = false
    end.first.resize_to_fit(WIDTH/5,HEIGHT/4).transparent('white')
    desc = Magick::Image.read("caption:#{forecast.next_desc}") do
      self.antialias = false
      self.fill = 'black'
      self.stroke = 'black'
      self.background_color = 'none'
      self.font = "Helvetica"
      self.size = "#{4*WIDTH/5}x#{HEIGHT/4}"
    end.first

    south = Magick::Image.new(WIDTH, HEIGHT/4)
    south.composite!(icon, Magick::WestGravity, Magick::OverCompositeOp)
    south.composite!(desc, Magick::EastGravity, Magick::OverCompositeOp)

    image = background.composite(south, Magick::SouthGravity, Magick::OverCompositeOp)

    prep_image(image)
  end


  post '/tagid/:tagid' do
    tagid = params[:tagid]
    "Got #{tagid}"
  end

  post '/:agent/image.?:format?' do
    agent = params[:agent]
    format = params[:format] || 'png'

    image = Magick::Image.read(params['file'][:tempfile].path) do
      self.antialias = false
      self.background_color = 'white'
      self.gravity = Magick::CenterGravity
    end.first.resize_to_fit(WIDTH, HEIGHT).extent(WIDTH, HEIGHT)

    send_image(image, agent)
    if format == 'txt'
      interlace(pixels).unpack("H*").to_s
    else
      #Give the user the png version
      content_type 'image/png'
      image.format = 'png'
      image.to_blob
    end

  end

  get '/:agent/chart' do
    url = params[:url] || 'https://staging.newrelic.com/public/charts/fAmWyK06eYB'
    file = Tempfile.new('chart')
    path = file.path + '.png'

    Screencap::Fetcher.new(url).fetch(
      output: path,
      div: '#embedded-chart',
      width: WIDTH,
      height: HEIGHT,
    )

    image = Magick::Image.read(path) do
      self.background_color = 'white'
      self.antialias = false
    end.first

    image.colorspace = Magick::GRAYColorspace
    image.image_type = Magick::BilevelType

    image = image.resize_to_fit(WIDTH, HEIGHT).extent(WIDTH, HEIGHT)

    #send_image(image, agent)
    #Give the user the png version
    content_type 'image/png'
    image.format = 'png'
    image.to_blob
  end

  post '/text.?:format?' do
    body = JSON.parse(request.body.read.to_s)
    msg = body['msg'] || 'hello world'
    format = params[:format] || 'epd'
    image = text2image(msg)

    if format == 'png'
      #Give the user the png version
      content_type 'image/png'
      image.format = 'png'
      image.to_blob
    else
      pixels = image.rotate(180).negate.export_pixels(0, 0, WIDTH, HEIGHT, 'I')
      interlace(pixels)
    end
  end

  get '/:agent/text.?:format?' do
    #agent url, text
    msg = params[:msg] || 'hello world'
    agent = params[:agent]
    format = params[:format] || 'png'
    image = text2image(msg)

    if format == 'png'
      #Give the user the png version
      content_type 'image/png'
      image.format = 'png'
      image.to_blob
    else
      #+2's to compensate for padding
      pixels = image.rotate(180).negate.export_pixels(0, 0, WIDTH+2, HEIGHT+2, 'I')
      interlace(pixels)
    end
  end

  def text2image(msg)
    Magick::Image.read("caption:#{msg}") do
      self.colorspace = Magick::GRAYColorspace
      self.image_type = Magick::BilevelType
      self.background_color = 'white'
      self.fill = 'black'
      self.stroke = 'black'
      self.size = "#{WIDTH}x#{HEIGHT}"
      self.font = "Helvetica"
      self.antialias = false
      self.gravity = Magick::CenterGravity
    end.first
  end

  def prep_image(image)
    image.colorspace = Magick::GRAYColorspace
    image.image_type = Magick::BilevelType
    #Compensate for padding
    image.border(1, 1, 'white')
  end

  def send_image(image, agent)
    agent_url = "https://agent.electricimp.com/#{agent}/image"
    #+2's to compensate for padding
    pixels = image.rotate(180).negate.export_pixels(0, 0, WIDTH+2, HEIGHT+2, 'I')

    options = {
      body: interlace(pixels)
    }
    HTTParty.post(agent_url, options)
  end

  def interlace(pixels)
    [pixels.collect{|p| [1, p].min}.join].pack("B*")
  end

end
